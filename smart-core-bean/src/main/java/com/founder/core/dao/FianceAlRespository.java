package com.founder.core.dao;

import com.founder.core.domain.FianceAl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public interface FianceAlRespository extends JpaRepository<FianceAl, Integer> {

    @Modifying
    @Query(value = "delete from FianceAl where mchid = :mchId and date = :date")
    int deleteAllByMchidAndDate(@Param(value = "mchId") String mchId, @Param(value = "date") String date);

    @Query(value = "from FianceAl where mchid = :mchId and date = :date and bzorder like %:keyName% ")
    List<FianceAl> queryAllByMchidAndBzorder(@Param(value = "mchId") String mchId, @Param(value = "date") String date, @Param(value = "keyName") String keyName);
}

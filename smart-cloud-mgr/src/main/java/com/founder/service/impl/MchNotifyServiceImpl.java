package com.founder.service.impl;

import com.founder.core.dao.MchNotifyRespository;
import com.founder.core.domain.MchNotify;
import com.founder.core.log.MyLog;
import com.founder.service.IMchNotifyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

@Service
public class MchNotifyServiceImpl implements IMchNotifyService {

    private static final MyLog _log = MyLog.getLog(MchNotifyServiceImpl.class);

    @Autowired
    MchNotifyRespository mchNotifyRespository;

    @Override
    public MchNotify selectMchNotify(String orderId) {
        return mchNotifyRespository.getOne(orderId);
    }

    @Override
    public Page<MchNotify> selectMchNotifyList(int offset, int limit, MchNotify mchNotify) {
        _log.info("分页查询商户通知列表，offset={}，limit={}。", offset, limit);
        Pageable pageable = PageRequest.of(offset, limit);
        Page<MchNotify> page = mchNotifyRespository.findAll(new Specification<MchNotify>() {
            @Override
            public Predicate toPredicate(Root<MchNotify> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = criteriaBuilder.conjunction();
                if (mchNotify != null){
                    String orderId = mchNotify.getOrderId();
                    if (StringUtils.isBlank(orderId)){
                        _log.info("传入订单号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("orderId"), "%"));
                    } else {
                        _log.info("按照订单号模糊查询商户通知");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("orderId"), "%"+mchNotify.getOrderId().trim()+"%"));
                    }
                    String mchOrderNo = mchNotify.getMchOrderNo();
                    if (StringUtils.isBlank(mchOrderNo)){
                        _log.info("传入商户单号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchOrderNo"), "%"));
                    } else {
                        _log.info("按照商户订单号模糊查询商户通知");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchOrderNo"), "%"+mchNotify.getMchOrderNo().trim()+"%"));
                    }
                    Integer status = mchNotify.getStatus();
                    if (status != null && status != -99){
                        _log.info("订单状态-99标识全部");
                        predicate.getExpressions().add(criteriaBuilder.equal(root.get("status"), mchNotify.getStatus()));
                    }
                }
                criteriaQuery.where(predicate);
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get("createTime").as(Date.class)));
                return criteriaQuery.getRestriction();
            }
        }, pageable);
        return page;
    }

    @Override
    public List<MchNotify> getMchNotifyList(int offset, int limit, MchNotify mchNotify) {
        Pageable pageable = PageRequest.of(offset,limit);
        Example<MchNotify> example = Example.of(mchNotify);
        Page<MchNotify> page = mchNotifyRespository.findAll(example,pageable);
        return page.getContent();
    }

    @Override
    public Integer count(MchNotify mchNotify) {
        Example<MchNotify> example = Example.of(mchNotify);
        Long count = mchNotifyRespository.count(example);
        return count.intValue();
    }
}

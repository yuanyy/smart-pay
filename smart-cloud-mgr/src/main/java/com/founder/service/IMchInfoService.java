package com.founder.service;

import com.founder.core.domain.MchInfo;

import java.util.List;

public interface IMchInfoService {

    /**
     * 查询商户数量
     * @param mchInfo
     * @return
     */
    Integer count(MchInfo mchInfo);

    /**
     * 分页查询
     * @param offset
     * @param limit
     * @param mchInfo
     * @return
     */
    List<MchInfo> getMchInfoList(int offset, int limit, MchInfo mchInfo);

    /**
     * 按照商户号查询
     * @param mchId
     * @return
     */
    MchInfo selectMchInfo(String mchId);

    /**
     * 增加
     * @param mchInfo
     * @return
     */
    int addMchInfo(MchInfo mchInfo);

    /**
     * 更新
     * @param mchInfo
     * @return
     */
    int updateMchInfo(MchInfo mchInfo);
}

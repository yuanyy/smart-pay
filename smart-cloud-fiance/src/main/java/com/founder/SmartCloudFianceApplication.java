package com.founder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartCloudFianceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartCloudFianceApplication.class, args);
    }
}
